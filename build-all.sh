#!/usr/bin/env bash

set -e

commit="$(git rev-parse HEAD)"
images="$@"

dhall to-directory-tree --output dockerfiles < dockerfiles.dhall

cd dockerfiles
if [ -z "$images" ]; then
    images="$(ls)"
fi

for d in $images; do
  tag="registry.gitlab.haskell.org/ghc/ci-images/$(basename $d):$commit"
  docker build -t "$tag" "$d"
  docker push "$tag"
done
