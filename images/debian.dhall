-- Debian/Ubuntu Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Llvm = ../components/Llvm.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  Image = ../Image.dhall

let
  docker_base_url: Text = "registry.gitlab.haskell.org/ghc/ci-images"

let
  coreBuildDepends: List Text =
    [ "zlib1g-dev"
    , "libtinfo-dev"
    , "libsqlite3-0"
    , "libsqlite3-dev"
    , "libgmp-dev"
    , "libncurses-dev"
    , "ca-certificates"
    , "g++"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "gcc"
    , "perl"
    , "python3"
    , "texinfo"
    , "xz-utils"
    , "lbzip2"
    , "bzip2"
    , "patch"
    , "openssh-client"
    , "sudo"
    , "time"
    , "jq"
    , "wget"
    , "curl"
    , "locales"
    -- DWARF libraries
    , "libdw1", "libdw-dev"
    -- For nofib
    , "valgrind"
    -- For the dtrace code generator and headers
    , "systemtap-sdt-dev"
    -- For lndir
    , "xutils-dev"
    -- For wasm CI jobs
    , "unzip"
    -- For python
    , "pkg-config"
    ]

let
  docsBuildDepends: List Text =
    [ "python3-sphinx"
    , "texlive-xetex"
    , "texlive-latex-extra"
    , "texlive-binaries"
    , "texlive-fonts-recommended"
    , "lmodern"
    , "tex-gyre"
    ]

let debianBuildDepends: List Text =
    [ "texlive-generic-extra"
    ]

let debian11OrHigherBuildDepends: List Text =
    [ "texlive-plain-generic"
    ]

let debian12LinuxPerfDepends: List Text =
    [ "linux-perf"
    ]


let ubuntuBuildDepends: List Text =
    [ "texlive-plain-generic"
    ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let
  RepoType = < Standard | Archive >

let
  updateRepositories: RepoType -> CF.Type =
    \(repo : RepoType) ->
      let useArchives : CF.Type =
        [ CF.Statement.Comment "Use archive repos"
        , CF.Statement.Exec [ "sed", "-i", "s/deb.debian.org/archive.debian.org/g", "/etc/apt/sources.list" ]
        , CF.Statement.Exec [ "sed", "-i", "s|security.debian.org|archive.debian.org/|g", "/etc/apt/sources.list" ]
        , CF.Statement.Exec [ "sed", "-i", "/-updates/d", "/etc/apt/sources.list" ]
        , CF.Statement.Empty
        ]

      in merge { Standard = [] : CF.Type, Archive = useArchives } repo

let
  installPackages: List Text -> CF.Type =
    \(pkgs: List Text) ->
        CF.arg (toMap { DEBIAN_FRONTEND = "noninteractive" })
      # CF.run "install build dependencies"
        [ "apt-get update"
        , "apt-get install --no-install-recommends -qy ${Prelude.Text.concatSep " " pkgs}"
        ]

let
  cleanApt: CF.Type =
      CF.run "clean apt cache"
      [ "apt-get clean"
      , "rm -rf /var/lib/apt/lists/*"
      ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --gecos 'GHC builds' --disabled-password"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

-- Install stack for testing hadrian
let
  installStack: CF.Type =
      CF.run "install stack"
      [ "curl -sSL https://get.haskellstack.org/ | sh"
      ]

let
  installLlvmFromApt : Text -> Optional Llvm.Config =
      \(ver : Text) -> Some
          { action = CF.run "install llvm" [ "apt-get install -qy wget software-properties-common gnupg lsb-release"
                                                         , "wget https://apt.llvm.org/llvm.sh"
                                                         , "chmod +x llvm.sh"
                                                         , "./llvm.sh ${ver}"
                                                         ]
          , out_llc = "/usr/bin/llc-${ver}"
          , out_opt = "/usr/bin/opt-${ver}"
          , out_clang = "/usr/bin/clang-${ver}"
          }

  -- Workaround for https://github.com/llvm/llvm-project/issues/62475
let
  installLlvmFromAptBookworm : Text -> Optional Llvm.Config =
      \(ver : Text) -> Some
          { action = CF.run "install llvm" [ "apt-get install -qy wget software-properties-common gnupg lsb-release"
                                                         , "echo \"deb http://apt.llvm.org/bookworm/ llvm-toolchain-bookworm-${ver} main\" > /etc/apt/sources.list.d/apt.llvm.org.list"
                                                         , "wget -qO- https://apt.llvm.org/llvm-snapshot.gpg.key |  tee /etc/apt/trusted.gpg.d/apt.llvm.org.asc"
                                                         , "apt update"
                                                         , "apt-get install -y clang-${ver} lldb-${ver} lld-${ver} clangd-${ver}"
                                                         ]
          , out_llc = "/usr/bin/llc-${ver}"
          , out_opt = "/usr/bin/opt-${ver}"
          , out_clang = "/usr/bin/clang-${ver}"
          }


let
  installEMSDK: CF.Type =
      CF.run "install emsdk"
      [ "sudo mkdir /emsdk"
      , "sudo chown ghc:ghc /emsdk"
      , "curl -f -L --retry 5 https://github.com/emscripten-core/emsdk/archive/refs/tags/3.1.40.tar.gz | tar xz --strip-components=1 -C /emsdk"
      , "/emsdk/emsdk install latest"
      , "/emsdk/emsdk activate latest"
      ]
      # CF.env (toMap { EMSDK = "/emsdk" })

let
  installNodeJS: CF.Type =
      CF.run "install nodejs"
      [ "curl -fsSL https://deb.nodesource.com/setup_21.x | sudo -E bash - && sudo apt-get install -y nodejs"
      ]

let
  installGoogleClosureCompiler: CF.Type =
      CF.run "install Google Closure Compiler"
      [ "sudo npm i -g google-closure-compiler@20240317.0.0"
      -- By some reason "linux native" version ends with segmentation fault on deb11.
      -- But we can use java version w/o issues.
      -- To run Google Closure Compiler in "java"-platform mode needed call:
      -- $ google-closure-compiler --platform=java
      -- For example:
      -- $ google-closure-compiler --platform=java --help
      , "sudo apt install -y default-jre"
      ]

let
  installLatestPython: CF.Type =
      CF.run "install latest python for testsuite"
      [
        "unset LLC OPT"
      , "cd $(mktemp -d)"
      , "curl -f -L --retry 5 https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tar.xz | tar xJ --strip-components=1"
      , "./configure --build=$(dpkg --print-architecture)-linux --prefix=/home/ghc/.local"
      , "make install -j$(curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc/-/raw/master/mk/detect-cpu-count.sh | sh)"
      , "sudo rm -rf /tmp/*"
      ]
      # CF.env (toMap { PYTHON = "/home/ghc/.local/bin/python3" })

let
  installLibzstd: CF.Type =
      CF.run "install libzstd >= 1.4.0, required for IPE data compression"
      [ "sudo mkdir /libzstd"
      , "sudo chown ghc:ghc /libzstd"
      , "curl -f -L --retry 5 https://github.com/facebook/zstd/releases/download/v1.5.5/zstd-1.5.5.tar.gz | tar xz --strip-components=1 -C /libzstd"
      , "cd /libzstd/lib"
      , "sudo make prefix=/usr install -j$(curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc/-/raw/master/mk/detect-cpu-count.sh | sh)"
      ]

let
  bootLlvmDir: Text = "/opt/llvm-bootstrap"
let
  bootLlvmConfig = \(bd : Llvm.BindistSpec) -> Some (Llvm.installFromBindistTo bootLlvmDir bd)
let
  llvmDir: Text = "/opt/llvm"
let
  llvmConfig = \(bd : Llvm.BindistSpec) -> Some (Llvm.installFromBindistTo llvmDir bd)

let
  DebianImage =
    let
      type: Type =
        { name: Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapLlvm : Optional Llvm.Config
        , bootstrapGhc : Ghc.BindistSpec
        , llvm : Optional Llvm.Config
        , cabalSource : Cabal.Type
        , extraPackages: List Text
          -- N.B. Optional as Stack's installer does not support all platforms
        , withStack : Bool
        , withEMSDK : Bool
        , withClosure : Bool
        -- deb9/ubuntu18 python too old for testsuite driver, needs at least 3.7
        , withLatestPython : Bool
        , withLibzstd : Bool
        , repoType : RepoType
        }

    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"
        let
          bootstrapLlvmConfigureOptions = Llvm.configureArgs opts.bootstrapLlvm


        let
          image =
            CF.from opts.fromImage
          # CF.env (toMap { LANG = "C.UTF-8" })
          # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
          # updateRepositories opts.repoType
          # installPackages (buildDepends # opts.extraPackages)
          # (if opts.withStack then installStack else [] : CF.Type)

            -- install LLVM for bootstrap GHC
          # Llvm.install opts.bootstrapLlvm

            -- install GHC
          # Ghc.install
              { bindist = Ghc.Bindist.BindistSpec opts.bootstrapGhc
              , destDir = ghcDir
              , configureOpts = bootstrapLlvmConfigureOptions
              }
          # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

            -- install LLVM to be used by built compiler
          # Llvm.install opts.llvm
          # Llvm.setEnv  opts.llvm

          # cleanApt

            -- install cabal-install
          # Cabal.install opts.cabalSource

            -- install hscolour, alex, and happy
          # CF.run "update cabal index" [ "$CABAL update"]
          # HaskellTools.installGhcBuildDeps

          # createUserStep
          # (if opts.withEMSDK || opts.withClosure then installNodeJS else [] : CF.Type)
          # (if opts.withEMSDK then installEMSDK else [] : CF.Type)
          # (if opts.withClosure then installGoogleClosureCompiler else [] : CF.Type)
          # (if opts.withLatestPython then installLatestPython else [] : CF.Type)
          # (if opts.withLibzstd then installLibzstd else [] : CF.Type)
          # [ CF.Statement.Cmd ["bash"] ]

        in Image::{ name = opts.name, runnerTags = opts.runnerTags, image = image }
    in
    { Type = type
    , default =
      { withStack = False
      , withEMSDK = False
      , withClosure = False
      , withLatestPython = False
      , withLibzstd = False
      , repoType = RepoType.Standard
      , bootstrapLlvm = None Llvm.Config
      , llvm = None Llvm.Config
      }
    , toDocker = toDocker
    }

let debian11Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb11"
    , fromImage = "amd64/debian:bullseye"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb10-linux" }
    , llvm = installLlvmFromApt "15"
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-deb10" }
    , extraPackages =
          debian11OrHigherBuildDepends
        -- For cross-compilation testing
        # [ "crossbuild-essential-arm64", "crossbuild-essential-s390x", "qemu-user" ]
    , withEMSDK = False
    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb11-emsdk-closure"
    , fromImage = "amd64/debian:bullseye"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb10-linux" }
    , llvm = installLlvmFromApt "15"
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-deb10" }
    , extraPackages = [] : List Text
    , withEMSDK = True
    , withClosure = True
    }
, DebianImage.toDocker DebianImage::
    { name = "armv7-linux-deb11"
    , fromImage = "arm32v7/debian:bullseye"
    , runnerTags = [ "armv7-linux" ]
    , bootstrapLlvm = bootLlvmConfig { version = "12.0.1", triple = "armv7a-linux-gnueabihf" }
    , bootstrapGhc = { version = "9.4.3", triple = "armv7-deb10-linux" }
    -- MP: I guess this won't work, but getting newer version that 13 is hard because the bindists are
    -- built against newer glibc. Perhaps get it from llvm-13 package?
    , llvm = llvmConfig { version = "12.0.1", triple = "armv7a-linux-gnueabihf" }
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-armv7-linux-deb10.tar.xz")
    , extraPackages = [ "libnuma-dev", "libtinfo5" ] # debian11OrHigherBuildDepends
    }
, DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb11"
    , fromImage = "arm64v8/debian:bullseye"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "aarch64-deb10-linux" }
    , llvm = installLlvmFromApt "15"
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "aarch64-linux-deb10" }
    , extraPackages = [ ] : List Text
    }
, DebianImage.toDocker DebianImage::
    { name = "i386-linux-deb11"
    , fromImage = "i386/debian:bullseye"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.8.2", triple = "i386-deb10-linux" }
    , llvm = None Llvm.Config
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "i386-linux-deb9" }
    , extraPackages = debian11OrHigherBuildDepends
    }
]

let x86_deb12_base : DebianImage.Type =
  DebianImage::
    { name = "x86_64-linux-deb12"
    , fromImage = "amd64/debian:bookworm"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb10-linux" }
    , llvm = installLlvmFromAptBookworm "18"
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-deb11" }
    , extraPackages =
          debian11OrHigherBuildDepends
        -- Add support for perf test suite runs
        # debian12LinuxPerfDepends
    , withEMSDK = False
    , withClosure = False
    }


let debian12Images: List Image.Type =
[ DebianImage.toDocker
    (x86_deb12_base with extraPackages = x86_deb12_base.extraPackages # debian12LinuxPerfDepends # ["libnuma-dev"]
                    with withLibzstd = True
                    with withStack = True  )

, DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb12"
    , fromImage = "arm64v8/debian:bookworm"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "aarch64-deb10-linux" }
    , llvm = installLlvmFromApt "15"
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "aarch64-linux-deb11" }
    , extraPackages = [ ] : List Text
    }

, DebianImage.toDocker DebianImage::
    { name = "i386-linux-deb12"
    , fromImage = "i386/debian:bookworm"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.8.2", triple = "i386-deb10-linux" }
    , llvm = None Llvm.Config
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "i386-linux-deb9" }
    , extraPackages =
        debian11OrHigherBuildDepends
    }
]

let debian10Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "aarch64-linux-deb10"
    , fromImage = "arm64v8/debian:buster"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "aarch64-deb10-linux" }
    , llvm = installLlvmFromApt "15"
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "aarch64-linux-deb10" }
    , extraPackages = [] : List Text
    }
, DebianImage.toDocker DebianImage::
    { name = "armv7-linux-deb10"
    , fromImage = "arm32v7/debian:buster"
    , runnerTags = [ "armv7-linux" ]
    , bootstrapLlvm = bootLlvmConfig { version = "12.0.1", triple = "armv7a-linux-gnueabihf" }
    , bootstrapGhc = { version = "9.4.3", triple = "armv7-deb10-linux" }
    -- MP: I guess this won't work, but getting newer version that 13 is hard because the bindists are
    -- built against newer glibc. llvm-13 package doesn't exist on deb10 so perhaps we are stuck.
    , llvm = llvmConfig { version = "12.0.1", triple = "armv7a-linux-gnueabihf" }
    , cabalSource = (Cabal.Type.FromBindist "https://downloads.haskell.org/~ghcup/unofficial-bindists/cabal/3.6.2.0/cabal-install-3.6.2.0-armv7-linux-deb10.tar.xz")
    , extraPackages = [ "libnuma-dev", "libtinfo5", "lld" ] # debianBuildDepends
    }
    -- N.B. Need GHC bindist for deb10 i386
, DebianImage.toDocker DebianImage::
    { name = "i386-linux-deb10"
    , fromImage = "i386/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "i386-deb9-linux" }
    , llvm = None Llvm.Config
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "i386-linux-deb9" }
    , extraPackages =
        debianBuildDepends
      # [ "libnuma-dev", "libtinfo5" ]
      # [ "cabal-install" ]
    }
, DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb10"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb10-linux" }
    , llvm = installLlvmFromApt "15"
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-deb10" }
    , extraPackages =
          debianBuildDepends
        # [ "libnuma-dev" ]
    }
]

let debian9Images: List Image.Type =
[ DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-deb9"
    , fromImage = "amd64/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb9-linux" }
    -- No debian 9 build for cabal 3.10.1.0
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.1.0", triple = "x86_64-linux-deb9" }
    , extraPackages = debianBuildDepends : List Text
    , withLatestPython = True
    , repoType = RepoType.Archive
    }
, DebianImage.toDocker DebianImage::
    { name = "i386-linux-deb9"
    , fromImage = "i386/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "i386-deb9-linux" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "i386-linux-deb9" }
    , extraPackages = debianBuildDepends # [ "libtinfo5", "cabal-install" ] : List Text
    , withLatestPython = True
    , repoType = RepoType.Archive
    }
]

let ubuntuImages: List Image.Type =
[
  DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu24_04"
    , fromImage = "amd64/ubuntu:24.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb11-linux" }
    , llvm = llvmConfig { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-deb11" }
    , extraPackages = ubuntuBuildDepends
    }
  , DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu22_04"
    , fromImage = "amd64/ubuntu:22.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb10-linux" }
    , llvm = llvmConfig { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-deb10" }
    , extraPackages = ubuntuBuildDepends
    }
  , DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu20_04"
    , fromImage = "amd64/ubuntu:20.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb10-linux" }
    , llvm = llvmConfig { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-ubuntu20_04" }
    , extraPackages = ubuntuBuildDepends
    }
  , DebianImage.toDocker DebianImage::
    { name = "x86_64-linux-ubuntu18_04"
    , fromImage = "amd64/ubuntu:18.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb9-linux" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-ubuntu18_04" }
    , extraPackages = [ "libtinfo5" ] # ubuntuBuildDepends
    , withLatestPython = True
    }
]

let linterImages: List Image.Type =
[ Image::
  { name = "linters"
  , runnerTags = [ "x86_64-linux" ]
  , jobStage = "build-derived"
  , needs = [ "x86_64-linux-deb12" ]
  , image =
      let installMypy: CF.Type =
          installPackages [ "pipx", "python3-setuptools", "python3-pip", "python3-dev", "python3-wheel" ]
        -- Need version 1.5.0.0 of pipx which isn't available on deb10
        # CF.run "installing mypy" [ "pipx install pipx" ]
        # CF.run "installing mypy" [ "/root/.local/bin/pipx install --global mypy==1.0" ]

      let lintersCommit: Text = "b376b0c20e033e71751fe2059daba1ba40b886be"
      let installShellcheck: CF.Type =
          installPackages [ "shellcheck" ]
      in
          CF.from "${docker_base_url}/x86_64-linux-deb12:latest"
        # [ CF.Statement.User "root" ]
        # installMypy
        # installShellcheck
        # [CF.Statement.User "ghc"]
  }
]

let bootstrapImages: List Image.Type =
[ DebianImage.toDocker (x86_deb12_base
    with name = "x86_64-linux-deb12-ghc9_2"
    with bootstrapGhc = { version = "9.2.8", triple = "x86_64-deb10-linux" })

, DebianImage.toDocker (x86_deb12_base
    with name = "x86_64-linux-deb12-ghc9_4"
    with bootstrapGhc = { version = "9.4.8", triple = "x86_64-deb10-linux" })

, DebianImage.toDocker (x86_deb12_base
    with name = "x86_64-linux-deb12-ghc9_6"
    with bootstrapGhc = { version = "9.6.4", triple = "x86_64-deb10-linux" })

, DebianImage.toDocker (x86_deb12_base
     with name = "x86_64-linux-deb12-ghc9_8"
     with bootstrapGhc = { version = "9.8.1", triple = "x86_64-deb10-linux" } )

, DebianImage.toDocker (x86_deb12_base
    with name = "x86_64-linux-deb12-ghc9_10"
    with bootstrapGhc = { version = "9.10.1", triple = "x86_64-deb10-linux" } )

]

let allImages: List Image.Type =
  linterImages # debian12Images # debian11Images # debian10Images # debian9Images # ubuntuImages # bootstrapImages

in allImages
