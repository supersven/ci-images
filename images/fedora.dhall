-- Fedora Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Llvm = ../components/Llvm.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  nsswitchWorkaround = ../components/NsswitchWorkaround.dhall
let
  Image = ../Image.dhall

let
  coreBuildDepends: List Text =
    [ "coreutils"
    , "binutils"
    , "which"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "diffutils"
    , "gcc"
    , "perl"
    , "python3"
    , "xz"
    , "pxz"
    , "bzip2"
    , "lbzip2"
    , "patch"
    , "openssh-clients"
    , "sudo"
    , "zlib-devel"
    , "sqlite"
    , "ncurses-compat-libs"
    , "gmp-devel"
    , "ncurses-devel"
    , "gcc-c++"
    , "findutils"
    , "curl"
    , "wget"
    , "jq"
    , "elfutils-libs"
    , "elfutils-devel"
    -- For the dtrace code generator and headers
    , "systemtap-sdt-devel"
    ]

let
  docsBuildDepends: List Text =
    [ "python3-pip"
    , "texinfo"
    , "texlive"
    , "texlive-latex"
    , "texlive-xetex"
    , "texlive-collection-latex"
    , "texlive-collection-latexrecommended"
    , "texlive-xetex-def"
    , "texlive-collection-xetex"
    , "python-sphinx-latex"
    , "dejavu-sans-fonts"
    , "dejavu-serif-fonts"
    , "dejavu-sans-mono-fonts"
    ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let
  installPackages: List Text -> CF.Type =
    \(pkgs: List Text) ->
      CF.run "install build dependencies"
        [ "dnf -y install ${Prelude.Text.concatSep " " pkgs}" ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --comment 'GHC builds'"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

let
  installLatestPython: CF.Type =
      CF.run "install latest python for testsuite"
      [
        "unset LLC OPT"
      , "cd $(mktemp -d)"
      , "curl -f -L --retry 5 https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tar.xz | tar xJ --strip-components=1"
      , "./configure --prefix=/home/ghc/.local"
      , "make install -j$(curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc/-/raw/master/mk/detect-cpu-count.sh | sh)"
      , "sudo rm -rf /tmp/*"
      ]
      # CF.env (toMap { PYTHON = "/home/ghc/.local/bin/python3" })

let
  bootLlvmDir: Text = "/opt/llvm-bootstrap"
let
  bootLlvmConfig = \(bd : Llvm.BindistSpec) -> Some (Llvm.installFromBindistTo bootLlvmDir bd)
let
  llvmDir: Text = "/opt/llvm"
let
  llvmConfig = \(bd : Llvm.BindistSpec) -> Some (Llvm.installFromBindistTo llvmDir bd)

let
  FedoraImage =
    let
      type: Type =
        { name: Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapLlvm : Optional Llvm.Config
        , bootstrapGhc : Ghc.BindistSpec
        , llvm : Optional Llvm.Config
        , cabalSource : Cabal.Type
        , extraPackages: List Text
        , withLatestPython : Bool
        }

    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"
        let
          bootstrapLlvmConfigureOptions = Llvm.configureArgs opts.bootstrapLlvm
        let
          image =
            CF.from opts.fromImage
          # CF.env (toMap { LANG = "C.UTF-8" })
          # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
          # installPackages (buildDepends # opts.extraPackages)

            -- install LLVM for bootstrap GHC
          # Llvm.install opts.bootstrapLlvm

            -- install GHC
          # Ghc.install
              { bindist = Ghc.Bindist.BindistSpec opts.bootstrapGhc
              , destDir = ghcDir
              , configureOpts = bootstrapLlvmConfigureOptions
              }
          # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

            -- install LLVM to be used by built compiler
          # Llvm.install opts.llvm
          # Llvm.setEnv opts.llvm

            -- install cabal-install
          # Cabal.install opts.cabalSource

            -- install hscolour, alex, and happy
          # CF.run "update cabal index" [ "$CABAL update"]
          # HaskellTools.installGhcBuildDeps

          # nsswitchWorkaround
          # createUserStep
          # (if opts.withLatestPython then installLatestPython else [] : CF.Type)
          # [ CF.Statement.Cmd ["bash"] ]

        in Image::{ name = opts.name, runnerTags = opts.runnerTags, image = image }
    in
    { Type = type
    , default =
      { withLatestPython = False
      , bootstrapLlvm = None Llvm.Config
      , llvm = None Llvm.Config
      , extraPackages = [] : List Text
      }
    , toDocker = toDocker
    }

let images: List Image.Type =
[ FedoraImage.toDocker FedoraImage::
    { name = "x86_64-linux-fedora33"
    , fromImage = "amd64/fedora:33"
    , runnerTags = [ "x86_64-linux" ]
    , llvm = llvmConfig { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" }
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-fedora33-linux" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-fedora33" }
    }
, FedoraImage.toDocker FedoraImage::
    { name = "x86_64-linux-fedora36"
    , fromImage = "amd64/fedora:36"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-fedora33-linux" }
    , llvm = llvmConfig { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-fedora33" }
    }
, FedoraImage.toDocker FedoraImage::
    { name = "x86_64-linux-fedora38"
    , fromImage = "amd64/fedora:38"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-fedora33-linux" }
    , llvm = llvmConfig { version = "15.0.6" , triple = "x86_64-linux-gnu-ubuntu-18.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-fedora33" }
    }
]
in images
