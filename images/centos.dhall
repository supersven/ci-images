-- Centos Docker images
let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  nsswitchWorkaround = ../components/NsswitchWorkaround.dhall
let
  Image = ../Image.dhall

let
  coreBuildDepends: List Text =
    [ "coreutils"
    , "binutils"
    , "which"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "gcc"
    , "perl"
    , "python3"
    , "texinfo"
    , "xz"
    , "pxz"
    , "lbzip2"
    , "bzip2"
    , "patch"
    , "openssh-clients"
    , "sudo"
    , "zlib-devel"
    , "sqlite"
    , "ncurses-compat-libs"
    , "gmp-devel"
    , "ncurses-devel"
    , "gcc-c++"
    , "findutils"
    , "curl"
    , "wget"
    , "jq"
    -- For the dtrace code generator
    , "systemtap-sdt-devel"
    ]

let
  docBuildDepends: List Text =
    [ "python-sphinx"
    , "texlive"
    , "texlive-latex"
    , "texlive-xetex"
    , "texlive-collection-latex"
    , "texlive-collection-latexrecommended"
    , "texlive-xetex-def"
    , "texlive-collection-xetex"
    , "python-sphinx-latex"
    , "dejavu-sans-fonts"
    , "dejavu-serif-fonts"
    , "dejavu-sans-mono-fonts"
    ]

let
  buildDepends: List Text = coreBuildDepends # docBuildDepends

-- systemd isn't running so remove it from nsswitch.conf
-- Failing to do this will result in testsuite failures due to
-- non-functional user lookup (#15230).
let
  nsswitchWorkaroundStep: CF.Type =
    CF.run "apply nsswitch workaround"
    [ "sed -i -e 's/systemd//g' /etc/nsswitch.conf" ]

let
  centosEOLWorkaroundStep: CF.Type =
    CF.run "patch .repo files"
    [ "sed -i s/mirror.centos.org/vault.centos.org/g /etc/yum.repos.d/*.repo"
    , "sed -i s/^#.*baseurl=http/baseurl=http/g /etc/yum.repos.d/*.repo"
    , "sed -i s/^mirrorlist=http/#mirrorlist=http/g /etc/yum.repos.d/*.repo" ]

let
  installDepsStep: CF.Type =
      CF.run "install build dependencies"
      [ "yum -y install ${Prelude.Text.concatSep " " buildDepends}" ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --comment 'GHC builds'"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"
    # CF.run "update cabal index" [ "$CABAL update"]

let
  installLatestPython: CF.Type =
      CF.run "install latest python for testsuite"
      [
        "unset LLC OPT"
      , "cd $(mktemp -d)"
      , "curl -f -L --retry 5 https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tar.xz | tar xJ --strip-components=1"
      , "./configure --prefix=/home/ghc/.local"
      , "make install -j$(curl -f -L --retry 5 https://gitlab.haskell.org/ghc/ghc/-/raw/master/mk/detect-cpu-count.sh | sh)"
      , "sudo rm -rf /tmp/*"
      ]
      # CF.env (toMap { PYTHON = "/home/ghc/.local/bin/python3" })

let
  CentosImage =
    let
      type: Type =
        { name : Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapGhc : Ghc.BindistSpec
        }
    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"

        let
          image: CF.Type =
          CF.from opts.fromImage
        # CF.env (toMap { LANG = "C.UTF-8" })
        # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
        # centosEOLWorkaroundStep
        # installDepsStep

        # nsswitchWorkaroundStep

          -- install GHC
        # Ghc.install
            { bindist = Ghc.Bindist.BindistSpec opts.bootstrapGhc
            , destDir = ghcDir
            , configureOpts = [] : List Text
            }
        # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

          -- install cabal-install
        # Cabal.install (Cabal.fromUpstreamBindist { version = "3.10.2.0", triple = "x86_64-linux-centos7" })

          -- install hscolour, alex, and happy
        # CF.run "update cabal index" [ "$CABAL update"]
        # HaskellTools.installGhcBuildDeps

        # nsswitchWorkaround
        # createUserStep
        # installLatestPython
        # [ CF.Statement.Cmd ["bash"] ]

        in
        Image::
        { name = opts.name
        , runnerTags = opts.runnerTags
        , image = image
        }
    in
    { Type = type
    , toDocker = toDocker
    }

let images: List Image.Type =
[ CentosImage.toDocker
    { name = "x86_64-linux-centos7"
    , fromImage = "centos:7"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapGhc = { version = "9.6.4", triple = "x86_64-centos7-linux" }
    }
]
in images
